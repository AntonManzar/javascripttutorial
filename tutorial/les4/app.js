const h = Vue.h;

const app = Vue.createApp({
    data() {
        return {
            title: 'Это свойство template'
        }
    },
    // template: `
    //   <div class="card center">
    //     <h1>{{ title }}</h1>
    //     <button class="btn" @click="title = 'Изменили'">Изменить</button>
    //   </div>
    // `
    methods: {
        changeTitle() {
            this.title = 'Изменили!'
        }
    },
    render() {
        return h('div', {
            class: 'card center'
        }, [
            h('h1', {}, this.title),
            h('button', {
                class: 'btn',
                onClick: this.changeTitle
            }, 'Изменить')
        ])
    }
})

app.mount('#app');